package com.funck.feign.client;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Set;

public interface BookResource {

    @RequestMapping(method = RequestMethod.GET)
    Set<Book> findAll();

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    Book findById(@PathVariable Long id);

    @RequestMapping(method = RequestMethod.POST)
    void save(@RequestBody Book book);

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    void update(@RequestBody Book book, @PathVariable Long id);

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    void deleteById(@PathVariable Long id);

}
