package com.funck.feign.client;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RequiredArgsConstructor
@RequestMapping("books")
@RestController
public class BookService implements BookResource {

    private final BookClient bookClient;

    public Set<Book> findAll() {
        return bookClient.findAll();
    }

    public Book findById(final Long id) {
        return bookClient.findById(id);
    }

    public void save(final Book book) {
        bookClient.save(book);
    }

    public void update(final Book book, final Long id) {
        bookClient.update(book, id);
    }

    public void deleteById(final Long id) {
        bookClient.deleteById(id);
    }

}
