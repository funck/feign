package com.funck.feign.client;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "books", url = "${server-url}")
public interface BookClient extends BookResource {

}
