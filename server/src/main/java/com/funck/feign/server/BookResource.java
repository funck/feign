package com.funck.feign.server;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.NoSuchElementException;
import java.util.Set;

@RequestMapping("books")
@RestController
public class BookResource {

    private static final Set<Book> books;

    static {
        books = new HashSet<>();
        books.add(new Book(1L, "Java como programar", "Deitel"));
        books.add(new Book(2L, "Clean code", "Robert Martin"));
        books.add(new Book(3L, "Clean architecture", "Robert Martin"));
    }

    @GetMapping
    public Set<Book> findAll() {
        return books;
    }

    @GetMapping("/{id}")
    public Book findById(@PathVariable final Long id) {
        return books.stream().filter(book -> book.getId().equals(id)).findFirst()
                .orElseThrow(NoSuchElementException::new);
    }

    @PostMapping
    public void save(@RequestBody final Book book) {
        Long nextId = books.stream().mapToLong(Book::getId).max().getAsLong();
        book.setId(nextId + 1);
        books.add(book);
    }

    @PutMapping("/{id}")
    public void update(@RequestBody final Book book, @PathVariable final Long id) {
        var savedBook = findById(id);
        savedBook.setAuthor(book.getAuthor());
        savedBook.setName(book.getName());
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable final Long id) {
        books.removeIf(book -> book.getId().equals(id));
    }

    @GetMapping("test")
    public String teste() {
        return "Test 2";
    }

}
